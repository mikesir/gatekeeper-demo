# Source repo for Gatekeeper demo 

This repo contains sample source for the demos used in my conference talk. I've used this source during the following talks:

- The 2021 Higher-Ed DevOps Conference
- The 2021 Virginia Alliance for Secure Computing and Networking (VASCAN) Conference
- Kubernetes Community Days Washington DC 2021

## Running the Demo

After cloning this repo, you can easily run through the demo yourself. I used the Kubernetes cluster bundled with Docker Desktop, but you are welcome to use any other cluster.

### Initial Setup

To run the demos, you'll need Gatekeeper and an ingress controller running. For simplicity, I have those bundled here and you can easily install them using the following command:

```bash
kubectl apply -k ./1-setup
```

### Applying the Gatekeeper Constraints

Once Gatekeeper is up and running, you can define a sample constraint template using the command below. This will eventually allow you to create `AuthorizedDomainPolicy` objects to limit what domains can be used within a namespace.

```bash
helm install gk-constraints ./2-gk-constraints
```

### Deploying a sample tenant

Using the Helm chart in the `/3-landlord` directory, you can define one or more tenants. Feel free to adjust the `values.yaml` to customize the tenant to your liking (or add more)! To keep things simpler for the demo, we aren't actually going to deploy Flux and connect all of the pieces (as that'll require other repos, SSH key configuration, etc.). 

```bash
helm install landlord ./3-landlord
```

Soon, you should see the `hello-world` namespace deployed (unless you renamed the tenant). In our normal setup, this will configure Flux to watch a manifest repo. For our demo, that "repo" is the `/hello-world` directory.

### Deploying the hello-world app

To deploy the hello-world app, simply deploy the manifests found in the `/hello-world` directory:

```bash
kubectl apply -k ./hello-world
```

To test the policy, try changing the `ingress.yaml` file to use other domains/hosts. When you attempt to apply the updated manifests, you'll see them fail.

## Cleaning up

To clean up, you can use the following commands:

```bash
helm uninstall landlord
helm uninstall gk-constraints
kubectl delete -k ./1-setup
```


## License/Usage

You are welcome to use any and all of the materials without attribution. Enjoy! But, we'd love to hear if you benefited from any examples you found. Reach out to Michael Irwin at mikesir@vt.edu and say hi!
