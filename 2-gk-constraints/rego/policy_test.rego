package authorizeddomainpolicy

test_single_domain_with_ingress {
  allowed_domains := ["test.com"]
  ingress_domains := ["test.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) == 0
}

test_single_domain_nonmatch_ingress {
  allowed_domains := ["other.com"]
  ingress_domains := ["test.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) != 0
}

test_wildcard_ingress {
  allowed_domains := ["*.test.com"]
  ingress_domains := ["subdomain.test.com", "test.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) == 0
}

test_wildcard_on_subdomain {
  allowed_domains := ["*.test.com"]
  ingress_domains := ["another-test.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) != 0
}

test_wildcard_nonmatch_ingress {
  allowed_domains := ["*.other.com"]
  ingress_domains := ["subdomain.test.com", "test.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) != 0
}

test_one_of_many_ingress {
  allowed_domains := ["test.com", "foo.com"]
  ingress_domains := ["test.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) == 0
}

test_one_of_many_nonmatch_ingress {
  allowed_domains := ["test.com", "foo.com"]
  ingress_domains := ["other.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) != 0
}

test_multi_match_ingress {
  allowed_domains := ["test.com", "foo.com", "bar.com"]
  ingress_domains := ["test.com", "foo.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) == 0
}

test_multi_match_nonmatch_ingress {
  allowed_domains := ["test.com", "foo.com", "bar.com"]
  ingress_domains := ["test.com", "other.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) != 0
}

test_subdomain_substring_ingress {
  allowed_domains := ["test.com"]
  ingress_domains := ["sometest.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) != 0
}

test_multilevel_wildcard {
  allowed_domains := ["*.example.com"]
  ingress_domains := ["test.subdomain.example.com"]
  input := input_obj(params_domains(allowed_domains), review_ingress(rules_domains(ingress_domains)))
  results := violation with input as input
  count(results) == 0
}


# ############# TEST CERTIFICATES ##############

test_single_domain_with_certificates {
  allowed_domains := ["test.com"]
  common_name := "test.com"
  dns_names := ["test.com"]
  input := input_obj(params_domains(allowed_domains), review_certificate(common_name, dns_names))
  results := violation with input as input
  count(results) == 0
}

test_single_domain_nonmatch_in_dns_name_certificate {
  allowed_domains := ["test.com"]
  common_name := "test.com"
  dns_names := ["test2.com"]
  input := input_obj(params_domains(allowed_domains), review_certificate(common_name, dns_names))
  results := violation with input as input
  count(results) == 1
}

test_single_domain_nonmatch_in_common_name_certificate {
  allowed_domains := ["test.com"]
  common_name := "test2.com"
  dns_names := ["test.com"]
  input := input_obj(params_domains(allowed_domains), review_certificate(common_name, dns_names))
  results := violation with input as input
  count(results) == 1
}

test_wildcard_full_match_certificate {
  allowed_domains := ["*.test.com"]
  common_name := "test.com"
  dns_names := ["subdomain.test.com", "test.com"]
  input := input_obj(params_domains(allowed_domains), review_certificate(common_name, dns_names))
  errors := violation with input as input
  count(errors) == 0
}

test_wildcard_nonmatch_in_all_names_certificate {
  allowed_domains := ["*.other.com"]
  common_name := "test.com"
  dns_names := ["subdomain.test.com", "test.com"]
  input := input_obj(params_domains(allowed_domains), review_certificate(common_name, dns_names))
  errors := violation with input as input
  count(errors) == 3
}

test_wildcard_nonmatch_in_dns_names_certificate {
  allowed_domains := ["*.other.com"]
  common_name := "test.com"
  dns_names := ["subdomain.other.com", "other.com"]
  input := input_obj(params_domains(allowed_domains), review_certificate(common_name, dns_names))
  errors := violation with input as input
  count(errors) == 1
}


############# HELPERS #############

review_ingress(rules) = out {
  out = {
    "kind": {
      "kind": "Ingress"
    },
    "object": {
      "kind": "Ingress",
      "apiVersion": "extensions/v1beta1",
      "metadata": {
        "name": "my-ingress",
      },
      "spec": {
        "rules": rules
      }
    }
  }
}

review_certificate(commonName, dnsNames) = out {
  out = {
    "kind": {
      "kind": "Certificate"
    },
    "object": {
      "kind": "Certificate",
      "apiVersion": "cert-manager.io/v1",
      "metadata": {
        "name": "my-certificate",
      },
      "spec": {
        "commonName": commonName,
        "dnsNames": dnsNames
      }
    }
  }
}

rules_domains(domain_list) = out {
  out = [
    rule_obj |
    rule_domain := domain_list[_]
    rule_obj := {"host": rule_domain}
  ]
}

params_domains(domains) = out {
  out = {
    "domains": domains
  }
}

input_obj(params, review) = out {
  out = {
    "parameters": params,
    "review": review
  }
}
