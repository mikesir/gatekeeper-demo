package authorizeddomainpolicy

violation[{"msg": msg}] {
	input.review.kind.kind == "Ingress"
	host := input.review.object.spec.rules[_].host
	valid_ingress_hosts := input.parameters.domains
	not fqdn_matches_any(host, valid_ingress_hosts)
	msg := sprintf("Unauthorized host on Ingress - %q", [host])
}

# Validate dnsNames on Certificate object
violation[{"msg" : msg}] {
	input.review.kind.kind == "Certificate"
	host := input.review.object.spec.dnsNames[_]
	valid_hosts := input.parameters.domains
	not fqdn_matches_any(host, valid_hosts)
	msg := sprintf("Unauthorized dnsName on Certificate - %q", [host])
}

# Validate commonName on Certificate object
violation[{"msg" : msg}] {
	input.review.kind.kind == "Certificate"
	host := input.review.object.spec.commonName
	valid_hosts := input.parameters.domains
	not fqdn_matches_any(host, valid_hosts)
	msg := sprintf("Unauthorized commonName on Certificate - %q", [host])
}

fqdn_matches_any(str, patterns) {
	fqdn_matches(str, patterns[_])
}

# Match *.<domain> against *.<domain> with support for multi-level wildcarding
fqdn_matches(str, pattern) {

	# Validate the pattern starts with *.
	pattern_parts := split(pattern, ".")
	pattern_parts[0] == "*"
	
	# Create an array that contains each segment of the pattern, minus the wildcard
	pattern_minus_wildcard := split(trim(pattern, "*."), ".")

	# Create an array of the incoming string, but to the same length of the pattern
	str_parts := split(str, ".")
	str_slice := array.slice(str_parts, count(str_parts) - count(pattern_minus_wildcard), count(str_parts))

	# Validate the two arrays are equal
	arrays_equal(str_slice, pattern_minus_wildcard)	
}

# Match *.<domain> against <domain>
fqdn_matches(str, pattern) {
	pattern_parts := split(pattern, ".")
	pattern_parts[0] == "*"
	suffix := trim(pattern, "*.")
	suffix == str
}

# Match <domain> against <domain>
fqdn_matches(str, pattern) {
	not contains(pattern, "*")
	str == pattern
}

arrays_equal(arr1, arr2) {
	not elements_differ(arr1, arr2)
}

elements_differ(arr1, arr2) {
	some i
	arr1[i] != arr2[i]
}
